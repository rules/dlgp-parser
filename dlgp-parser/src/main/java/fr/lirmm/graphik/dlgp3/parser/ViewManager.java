package fr.lirmm.graphik.dlgp3.parser;

import fr.inria.integraal.view_parser.parser.ViewDefinition;
import fr.inria.integraal.view_parser.parser.ViewDocument;
import fr.inria.integraal.view_parser.parser.ViewParseException;
import fr.inria.integraal.view_parser.parser.ViewParser;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

public class ViewManager {

    private final Map<String, Map<String, Integer>> predicatesByPrefix = new HashMap<>();

    public void addPrefix(String prefix, String filePath) throws ParseException {
        if(filePath.startsWith("<"))
            filePath=filePath.substring(1,filePath.length()-1);

        Map<String, Integer> viewsForFile = this.predicatesByPrefix.getOrDefault(prefix, new HashMap<>());

        ViewDocument mapping;
        try {
            mapping = ViewParser.parse(filePath);
        } catch (IOException e) {
            throw new ParseException("An error occurred while reading the file at " + filePath + "\n" + e);
        } catch (ViewParseException e) {
            throw new ParseException("An error occurred while parsing the file at " + filePath + "\n" + e);
        } catch (URISyntaxException e) {
            throw new ParseException("An iternal error occurred while parsing the file at " + filePath + "\n" + e);
        }
        for(ViewDefinition vd : mapping.getMappings()) {
            String label = vd.getId();
            Integer arity = vd.getTemplates().size();
            viewsForFile.put(label, arity);
        }
        this.predicatesByPrefix.put(prefix, viewsForFile);
    }

    public boolean isDefined(String prefix) {
        return this.predicatesByPrefix.containsKey(prefix);
    }

    public boolean isDefined(String prefix, String predicate, Integer arity) throws ParseException {
        if(this.isDefined(prefix)) {
            Map<String, Integer> predicatedForView = this.predicatesByPrefix.get(prefix);
            if(predicatedForView.containsKey(predicate)) {
                Integer definedArity = predicatedForView.get(predicate);
                if(definedArity.equals(arity)) {
                    return true;
                } else {
                    throw new ParseException("Predicate " + predicate + " was found on view " +  prefix + " but with the wrong arity : defined was " + definedArity + " but used was " + arity);
                }
            } else {
                throw new ParseException("Predicate " + predicate + " was not found on view " +  prefix);
            }
        } else {
            throw new ParseException("View " + prefix + " was not defined");
        }
    }


}
