package fr.lirmm.graphik.dlgp3.parser;


public class BuildException extends Exception{
	  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BuildException(String exception) {
		  super(exception);
	  }
}
