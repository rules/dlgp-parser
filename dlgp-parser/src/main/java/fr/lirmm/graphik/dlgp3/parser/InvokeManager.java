package fr.lirmm.graphik.dlgp3.parser;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

/**
 * This class manages the invocation of functions and types based on prefixes and configurations.
 */

public class InvokeManager {

	private final ConfigFileReader confReader = new ConfigFileReader();
	public static Map<String, String> prefixUriMap = new HashMap<>();
	public static Map<Object, String> inverseTypesTable = new HashMap<>();
	public static Map<String, Object> typesTable = new HashMap<>();
	public static Map<String, Object> prefixTable = new HashMap<>();
	public static Map<String, String> classToPath = new HashMap<>();

	/**
	 * Default constructor initializes the prefix table with special predicates and functions
	 * and the typesTable with standard types.
	 */
	public InvokeManager() {
		reverseMap();
	}

	/**
	 * Adds a prefix and the object that allows us to call functions in the prefixTable,
	 * and adds a (key, value) pair in prefixUriMap.
	 *
	 * @param prefix the prefix to add
	 * @param filePath the file path associated with the prefix
	 */
	public void addPrefix(String prefix, String filePath)
			throws IOException, ParseException, ClassNotFoundException {
		if (filePath.startsWith("<")) {
			filePath = filePath.substring(1, filePath.length() - 1);
		}
		getObjectToInvoke(prefix, filePath);
		prefixUriMap.put(prefix, filePath + "#");
	}

	/**
	 * Returns the object that allows to invoke given a prefix.
	 *
	 * @param key the key to search in the prefix table
	 * @return the object associated with the key
	 */
	public Object getInvokerObject(String key) {
		if (prefixTable.containsKey(key)) {
			return prefixTable.get(key);
		} else {
			int indexOf = key.indexOf("#");
			String prefix = key.substring(0, indexOf + 1);
			return prefixTable.get(prefix);
		}
	}

	/**
	 * Returns the full URI associated with a prefix.
	 *
	 * @param prefix the prefix to search
	 * @return the full URI associated with the prefix
	 */
	public String getFull(String prefix) {
		return prefixUriMap.get(prefix);
	}

	/**
	 * Returns the Java type given a Graal type.
	 *
	 * @param key the Graal type key to search
	 * @return the Java type associated with the key
	 */
	public Object getType(String key) {
		if (typesTable.containsKey(key)) {
			return typesTable.get(key);
		} else {
			int indexOf = key.indexOf("#");
			String prefix = key.substring(0, indexOf);
			return typesTable.get(prefix);
		}
	}

	/**
	 * Returns the Graal type given a Java type.
	 *
	 * @param key the Java type key to search
	 * @return the Graal type associated with the key
	 */
	public String getGraalType(Object key) {
		return inverseTypesTable.get(key);
	}


	/**
	 * Given a path for a config file, this method returns an instance of the class where the functions are defined.
	 *
	 * @param prefix the prefix to associate with the config
	 * @param prefixFilePath the path to the config file
	 */
	public void getObjectToInvoke(String prefix, String prefixFilePath)
			throws IOException, ParseException, ClassNotFoundException {
		JSONObject jsonObject = this.confReader.readJsonFile(prefixFilePath);

		if (jsonObject != null) {
			JSONObject defaultJsonObject = this.confReader.getDefault(jsonObject);
			JSONObject elementsJsonObject = this.confReader.getElements(jsonObject);

			for (Object key : elementsJsonObject.keySet()) {
				JSONObject currentJsonObject = (JSONObject) elementsJsonObject.get(key);
				String type = this.confReader.getType(currentJsonObject);
				JSONObject locationJsonObject = this.confReader.getLocation(currentJsonObject);

				switch (type) {
					case "literal":
						typesTable.put(prefix + key, this.confReader.getClassFromJsonObject(locationJsonObject));
						break;
					case "predicate":
					case "function":
						prefixTable.put(prefix + key, this.confReader.getClassFromJsonObject(locationJsonObject));
						break;
					default:
						throw new RuntimeException(type + " not in <literal,predicate,function>");
				}
			}

			if (!defaultJsonObject.isEmpty()) {
				JSONObject typesJsonObject = this.confReader.getTypes(defaultJsonObject);
				JSONObject predicatesJsonObject = this.confReader.getPredicates(defaultJsonObject);
				JSONObject functionsJsonObject = this.confReader.getFunctions(defaultJsonObject);

				if (!typesJsonObject.isEmpty()) {
					typesTable.put(prefix, this.confReader.getClassFromJsonObject(typesJsonObject));
				}
				if (!predicatesJsonObject.isEmpty()) {
					prefixTable.put(prefix, this.confReader.getClassFromJsonObject(predicatesJsonObject));
				}
				if (!functionsJsonObject.isEmpty()) {
					prefixTable.put(prefix, this.confReader.getClassFromJsonObject(functionsJsonObject));
				}
			}
		}
	}

	/**
	 * Reverses the typesTable map to create the inverseTypesTable map.
	 */
	private void reverseMap() {
		for (Map.Entry<String, Object> entry : typesTable.entrySet()) {
			inverseTypesTable.put(entry.getValue(), entry.getKey());
		}
	}
}
