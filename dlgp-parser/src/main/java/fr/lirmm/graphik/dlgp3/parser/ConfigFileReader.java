package fr.lirmm.graphik.dlgp3.parser;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ConfigFileReader {
	
	public ConfigFileReader() { }

	/**
	 * Given a JSON Object, this method returns an instance of the class
	 * using information in the JSON Object.
	 *
	 * @param jsonObject the JSON object containing class information
	 * @return the class object represented by the information in the JSON object
	 * @throws ClassNotFoundException if the given class is not found
	 * @throws IOException            if the given class file cannot be accessed
	 */
	public Class<?> getClassFromJsonObject(JSONObject jsonObject) throws ClassNotFoundException, IOException {
		Class<?> result;
		String className = this.getPackage(jsonObject) + "." + this.getClass(jsonObject);

		if (!jsonObject.containsKey("path")) {
			// Load the class directly from the classpath
			result = Class.forName(className);
		} else {
			String path = this.getPath(jsonObject);
			File directory = new File(path);

			if (!directory.exists() || !directory.isDirectory()) {
				throw new IOException("Invalid directory path: " + path);
			}

			// Create a URLClassLoader to load the class from the given directory
			URL url = directory.toURI().toURL();
			URL[] urls = new URL[]{url};

            try (URLClassLoader loader = new URLClassLoader(urls)) {
                result = loader.loadClass(className);
                InvokeManager.classToPath.put(className, path);
            } catch (ClassNotFoundException e) {
                // Try to compile and load the .java file if the .class file is not found
                Path javaFilePath = Paths.get(path, className.replace('.', File.separatorChar) + ".java");
                if (Files.exists(javaFilePath)) {
                    // Create a temporary directory for compiled .class files
                    File tempDir = createTempDirectory();
                    compileJavaFile(javaFilePath, tempDir.getAbsolutePath());

                    // Add the temporary directory to the class loader
                    URL tempUrl = tempDir.toURI().toURL();
                    URL[] tempUrls = new URL[]{tempUrl};

                    // Reload the class loader to include the newly compiled class
                    try (URLClassLoader tempLoader = new URLClassLoader(tempUrls)) {
                        result = tempLoader.loadClass(className);
                        InvokeManager.classToPath.put(className, tempDir.toString());
                    }
                } else {
                    throw new ClassNotFoundException("Neither .class nor .java file found for " + className);
                }
            }
		}

		return result;
	}

	/**
	 * Compiles a Java file.
	 *
	 * @param javaFilePath the path to the .java file
	 * @param outputDir the directory where the .class files should be placed
	 * @throws IOException if the file cannot be compiled
	 */
	private void compileJavaFile(Path javaFilePath, String outputDir) throws IOException {
		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		int result = compiler.run(null, null, null, "-d", outputDir, javaFilePath.toString());
		if (result != 0) {
			throw new IOException("Failed to compile .java file: " + javaFilePath);
		}
	}

	/**
	 * Creates a temporary directory.
	 *
	 * @return the temporary directory
	 * @throws IOException if the directory cannot be created
	 */
	private File createTempDirectory() throws IOException {
		File tempDir = new File(System.getProperty("java.io.tmpdir"), "compiledClasses_" + System.nanoTime());
		if (!tempDir.mkdir()) {
			throw new IOException("Could not create temp directory: " + tempDir.getAbsolutePath());
		}
		return tempDir;
	}

	/**
	 * Reads a JSON file and returns a JSONObject.
	 *
	 * @param URI the URI of the JSON file
	 * @return the parsed JSONObject
	 * @throws IOException    if the file cannot be read
	 * @throws ParseException if the file content cannot be parsed
	 */
	public JSONObject readJsonFile(String URI) throws IOException, ParseException {
		JSONObject jsonObj;
		try (Reader reader = new FileReader(URI)) {
			JSONParser parser = new JSONParser();
			jsonObj = (JSONObject) parser.parse(reader);
		}
		return jsonObj;
	}

	public JSONObject getDefault(JSONObject obj) {
		if(obj.containsKey("default"))
			return (JSONObject)obj.get("default");
		else
			return new JSONObject();
	}
	public JSONObject getTypes(JSONObject obj) {
		if(obj.containsKey("types"))
			return (JSONObject)obj.get("types");
		else
			return new JSONObject();
	}

	public JSONObject getPredicates(JSONObject obj) {
		if(obj.containsKey("predicates"))
			return (JSONObject)obj.get("predicates");
		else
			return new JSONObject();
	}

	public JSONObject getFunctions(JSONObject obj) {
		if(obj.containsKey("functions"))
			return (JSONObject)obj.get("functions");
		else
			return new JSONObject();
	}

	public JSONObject getElements(JSONObject obj) {
		if(obj.containsKey("elements"))
			return (JSONObject)obj.get("elements");
		else
			return new JSONObject();
	}

	public JSONObject getLocation(JSONObject obj) {
		if(obj.containsKey("location"))
			return (JSONObject)obj.get("location");
		else
			return new JSONObject();
	}

	public String getType(JSONObject obj) {
		return (String) obj.get("type");
	}

	public String getPath(JSONObject obj) {
		return (String) obj.get("path");
	}

	public String getPackage(JSONObject obj) {
		return (String) obj.get("package");
	}

	public String getClass(JSONObject obj) {
		return (String) obj.get("class");
	}
}
