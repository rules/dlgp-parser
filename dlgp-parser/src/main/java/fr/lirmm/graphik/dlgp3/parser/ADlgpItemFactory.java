package fr.lirmm.graphik.dlgp3.parser;

import javax.swing.event.EventListenerList;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public abstract class ADlgpItemFactory{

	public static enum OBJECT_TYPE {
		UNKNOWN, FACT, RULE, QUERY, NEG_CONSTRAINT
	}

	public static enum HEAD_BODY {
		UNKNOWN,HEAD,BODY
	}

	public static final String IRI="FULLIRI";
	public static final String IDENT="L_IDENT";

	public static final String XSD = "http://www.w3.org/2001/XMLSchema#";

	public static final String RDF = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";

	public static String DEFAULT_BASE = "http://www.lirmm.fr/dlgp/";
	public static String STANDARD_FUNCTION_PREFIX = "<stdfct>";

	public static InvokeManager invokeManager = new InvokeManager();

	public ViewManager viewManager = new ViewManager();

	protected boolean una = false;
	protected boolean isBaseDeclared = false;
	protected boolean isTopDeclared = false;


	protected OBJECT_TYPE current_object = OBJECT_TYPE.UNKNOWN,
			declared_object = OBJECT_TYPE.UNKNOWN;


	private PrefixManager prefixManager = new PrefixManager(DEFAULT_BASE);

	private ComputedManager computedManager = new ComputedManager();

	protected ArrayList < String > directives = new ArrayList < String > ();


	/** The listener list. */
	protected EventListenerList parserListenerList = new EventListenerList();



	/**
	 * Adds the parser listener.
	 *
	 * @param l the listener
	 */
	public void addParserListener(ParserListener l)
	{
		parserListenerList.add(ParserListener.class, l);
	}


	protected boolean isAbsolute(String s)
	{
		int colonIndex = s.indexOf(':');
		if (colonIndex == - 1) return false;
		else for (int i = 0; i < colonIndex; i++)
		{
			char ch = s.charAt(i);
			if (!Character.isLetterOrDigit(ch) && ch != '.'
					&& ch != '+' && ch != '-') return false;
		}
		return true;
	}

	protected Object getIRI(String s)
	{
		if (s.charAt(0) == '<')
		{
			s = s.substring(1, s.length() - 1);
			if (!isAbsolute(s)) s = prefixManager.getDefault() + s;
		}
		Object iri = this.createIRI(s);
		return iri;
	}

	public static String unescapeString(String s)
	{
		if (s.indexOf('\\') == - 1)
		{
			return s;
		}
		StringBuilder sb = new StringBuilder(s.length());
		for (int i = 0; i < s.length(); i++)
		{
			char ch = s.charAt(i);
			if (ch == '\\')
			{
				int j = i + 1;
				if (j < s.length())
				{
					char escCh = s.charAt(j);
					if (escCh == '\\' || escCh == '\"')
					{
						i++;
						sb.append(escCh);
					}
				}
				else
				{
					sb.append('\\');
				}
			}
			else
			{
				sb.append(ch);
			}
		}
		return sb.toString();
	}

	/**
	 * Removes the parser listener.
	 *
	 * @param l the listener
	 */
	public void removeParserListener(ParserListener l)
	{
		parserListenerList.remove(ParserListener.class, l);
	}

	/**
	 * Gets the parser listeners.
	 *
	 * @return the parser listeners
	 */
	public ParserListener [] getParserListeners()
	{
		return (ParserListener []) parserListenerList
				.getListeners(ParserListener.class);
	}

	protected void fireParseEvent(ParseEvent event)
	{
		// Guaranteed to return a non-null array
		Object [] listeners = parserListenerList.getListenerList();
		// Process the listeners last to first, notifying
		// those that are interested in this event
		for (int i = listeners.length - 2; i >= 0; i -= 2)
		{
			if (listeners [i] == ParserListener.class)
			{
				ParserListener pListener = (ParserListener) listeners [i + 1];
				event.fire(pListener);
			}
		}
	}

	protected class DirectiveEvent implements ParseEvent
	{
		String text;
		public DirectiveEvent(String txt)
		{
			this.text = txt;
		}

		public void fire(ParserListener listener)
		{
			listener.directive(text);
		}
	}

	protected class DeclarePrefixEvent implements ParseEvent
	{
		String prefix;
		String ns;
		public DeclarePrefixEvent(String prefix, String ns)
		{
			this.prefix = prefix;
			this.ns = ns;
		}

		public void fire(ParserListener listener)
		{
			listener.declarePrefix(prefix, ns);
		}
	}

	protected class DeclareTopEvent implements ParseEvent
	{
		String top;
		public DeclareTopEvent(String top)
		{
			this.top = top;
		}

		public void fire(ParserListener listener)
		{
			listener.declareTop(top);
		}
	}

	protected class DeclareBaseEvent implements ParseEvent
	{
		String base;
		public DeclareBaseEvent(String base)
		{
			this.base = base;
		}

		public void fire(ParserListener listener)
		{
			listener.declareBase(base);
		}
	}

	protected class DeclareViewEvent implements ParseEvent
	{
		String viewDefinitionFile;
		public DeclareViewEvent(String viewDefinitionFile)
		{
			this.viewDefinitionFile = viewDefinitionFile;
		}

		public void fire(ParserListener listener)
		{
			listener.declareView(viewDefinitionFile);
		}
	}

	protected class DeclareUNAEvent implements ParseEvent
	{
		public DeclareUNAEvent()
		{
		}

		public void fire(ParserListener listener)
		{
			listener.declareUNA();
		}
	}

	protected class StartsObjectEvent implements ParseEvent
	{
		OBJECT_TYPE objectType = null;
		String name;
		public StartsObjectEvent(OBJECT_TYPE objectType, String name)
		{
			this.objectType = objectType;
			this.name = name;
		}

		public void fire(ParserListener listener)
		{
			listener.startsObject(objectType, name);
		}
	}

	protected class ConjunctionEndsEvent implements ParseEvent
	{
		OBJECT_TYPE objectType = null;
		public ConjunctionEndsEvent(OBJECT_TYPE objectType)
		{
			this.objectType = objectType;
		}

		public void fire(ParserListener listener)
		{
			listener.endsConjunction(objectType);
		}
	}

	protected class FindsAtomEvent implements ParseEvent
	{
		Object predicate = null;
		Object [] terms = null;
		public FindsAtomEvent(Object predicate, Object [] terms)
		{
			this.predicate = predicate;
			this.terms = terms;
		}

		public void fire(ParserListener listener)
		{
			listener.createsAtom(predicate, terms);
		}
	}

	protected class FindsComputedAtomEvent implements ParseEvent
	{
		Object predicate = null;
		Object [] terms = null;
		public FindsComputedAtomEvent(Object predicate, Object [] terms)
		{
			this.predicate = predicate;
			this.terms = terms;
		}

		public void fire(ParserListener listener)
		{
			String prefix=(String) predicate;
			prefix=prefix.substring(0,prefix.indexOf(":")+1);
			listener.createsComputedAtom(predicate, terms,invokeManager);
		}
	}

	protected class FindsSpecialAtomEvent implements ParseEvent
	{
		Object leftTerm=null,rightTerm=null; String predicate;Object invokerObject;

		public FindsSpecialAtomEvent(Object leftTerm,String op,Object rightTerm) {
			this.leftTerm=leftTerm;
			this.predicate=getNameFromOperator(op);
			this.invokerObject=invokeManager;
			this.rightTerm=rightTerm;
		}

		public void fire(ParserListener listener)
		{
			listener.createsSpecialAtom(leftTerm, predicate,rightTerm,invokerObject);
		}
	}

	protected class FindsEqualityEvent implements ParseEvent
	{
		Object term1 = null, term2 = null;
		public FindsEqualityEvent(Object term1, Object term2)
		{
			this.term1 = term1;
			this.term2 = term2;
		}

		public void fire(ParserListener listener)
		{
			listener.createsEquality(term1, term2);
		}
	}

	protected class FindsNegativeConjunction implements ParseEvent{
		ArrayList<ArrayList<ParseEvent>> listNegConjunction;
		public FindsNegativeConjunction(ArrayList<ArrayList<ParseEvent>> listNegConjunction){
			this.listNegConjunction=listNegConjunction;
		}

		public void fire(ParserListener listener) {


			for(ArrayList<ParseEvent > listNegAtoms : listNegConjunction) {
				listener.beginNegation();
				for(ParseEvent Negatom:listNegAtoms) {
					fireParseEvent(Negatom);
				}
				listener.endNegation();
			}
		}
	}

	protected class FindsVarListEvent implements ParseEvent
	{
		Object [] varList = null;
		public FindsVarListEvent(Object [] varList)
		{
			this.varList = varList;
		}

		public void fire(ParserListener listener)
		{
			listener.answerTermList(varList);
		}
	}
	public interface ParseEvent
	{
		void fire(ParserListener listener);
	}

	public void setDefaultBase(String defaultBase)
	{
		prefixManager.setDefault(defaultBase);
	}

	public class PrefixManager
	{
		protected String defaultPrefix;

		protected HashMap < String, Object > prefixTable = new HashMap < String, Object > ();

		public PrefixManager(String defaultPrefix)
		{
			setDefault(defaultPrefix);
		}

		public String getDefault()
		{
			return defaultPrefix;
		}

		public void setDefault(String defaultPrefix)
		{
			this.defaultPrefix = defaultPrefix;
		}

		public Object getIRI(String qname)
		{
			return prefixTable.get(qname);
		}

		public boolean containsPrefix(String prefix)
		{
			return prefixTable.containsKey(prefix);
		}

		public void setPrefix(String prefix, Object iri)
		{
			prefixTable.put(prefix, iri);
		}
	}

	protected Object getIRIFromQName(String qname) throws BuildException
	{
		int colonIndex = qname.indexOf(':');
		if (colonIndex == - 1)
		{
			throw new BuildException(("Not a valid qname (missing ':') " + qname));
		}
		String prefix = qname.substring(0, colonIndex + 1);

		if(this.viewManager.isDefined(prefix)) {
			return this.getIRI(prefix + qname.substring(colonIndex + 1));
		}

		if ((!this.getComputedManager().containsPrefix(prefix))
				&& (!this.getPrefixManager().containsPrefix(prefix))) {
			throw new BuildException("Prefix not declared: " + prefix);
		}else {
			if(!this.getComputedManager().containsPrefix(prefix))
				return this.getIRI(this.getPrefixManager().getIRI(prefix) + qname.substring(colonIndex + 1));
			else
				return this.getComputedManager().getURI(prefix)+"#"+qname.substring(colonIndex + 1);
		}
	}


	protected Object getURI(String uri) throws ParseException
	{
		if (!uri.equals(ADlgpItemFactory.STANDARD_FUNCTION_PREFIX)) {
			try {
				invokeManager.addPrefix(getIRI(uri) + "#", uri);
			} catch (IOException | org.json.simple.parser.ParseException | ClassNotFoundException e) {
				throw new ParseException(e.getMessage());
			}
		}

		return uri;
	}

	protected Object getURIView(String uri)
	{
		return uri;
	}

	protected void declareView(String prefix, Object ns) throws BuildException, ParseException {
		String filePath = ns.toString();
		if (filePath.charAt(0) == '<') {
			filePath = filePath.substring(1, filePath.length() - 1);
		}

		this.viewManager.addPrefix(prefix, filePath);

		this.buildViewDecl(prefix, filePath);
	}

	public ParseEvent buildViewAtom(Object predicate, ArrayList<Object> listTerm) throws ParseException {
		String[] split = predicate.toString().split(":");
		if(this.viewManager.isDefined(split[0] + ':', split[1], listTerm.size())) {
			return this.buildAtom(split[1], listTerm);
		}
		return null;
	}

	protected class DeclareComputedEvent implements ParseEvent
	{
		String prefix;
		String ns;
		public DeclareComputedEvent(String prefix, String ns)
		{
			this.prefix = prefix;
			this.ns = ns;
		}

		public void fire(ParserListener listener)
		{
			listener.declareComputed(prefix, ns);
		}
	}


	public class ComputedManager
	{
		protected HashMap < String, Object > prefixTable = new HashMap < String, Object > ();
		public ComputedManager(){

		}

		public Object getURI(String qname)
		{
			return prefixTable.get(qname);
		}

		public boolean containsPrefix(String prefix)
		{
			return prefixTable.containsKey(prefix);
		}
		public void setPrefix(String prefix, Object uri)
		{
			prefixTable.put(prefix,uri);
		}
	}



	public void fireDirective(String text){
		this.fireParseEvent(this.instanceOfDirectiveEvent(text));
	}

	public boolean cantGuessUnknownType(String name) {
		if (this.current_object == OBJECT_TYPE.UNKNOWN)
		{
			if (this.declared_object != OBJECT_TYPE.UNKNOWN)
			{
				this.current_object = this.declared_object;
				this.parseEventConjunction(name);
				return false;
			}else {
				return true;
			}
		}else {
			return false;
		}


	}

	public void setCurrentObject(OBJECT_TYPE object_type) {
		this.current_object=object_type;
	}


	public void setDeclaredObject(OBJECT_TYPE object_type) {
		this.declared_object=object_type;
	}


	/*geter for the prefix and computed manager*/
	public PrefixManager getPrefixManager() {
		return this.prefixManager;
	}


	public ComputedManager getComputedManager() {
		return this.computedManager;
	}

	/*seters for the prefix and computed manager*/
	public void setPrefixManager(PrefixManager pm) {
		this.prefixManager=pm;
	}


	public  void setComputedManager(ComputedManager cm) {
		this.computedManager=cm;
	}

	/*we need it for the directive*/
	public ParseEvent instanceOfDirectiveEvent(String text) {
		return new DirectiveEvent(text);
	}

	public void parseEventConjunction(String name) {
		fireParseEvent(new StartsObjectEvent(current_object, name));
	}

	public Boolean isComputed(String token) {
		int indexOf=token.indexOf(":");
		String prefix=token.substring(0, indexOf+1);
		if (this.computedManager.containsPrefix(prefix))
			return true;
		else
			return false;
	}

	public Boolean isViewPredicate(String token) {
		int indexOf=token.indexOf(":");
		String prefix=token.substring(0, indexOf+1);
		if (this.viewManager.isDefined(prefix)) {
			return true;
		}
		else
			return false;
	}

	public String getNameFromOperator(String op) {
		switch (op) {
			case "=":
				return DEFAULT_BASE+"#eq";
			case "+":
				return DEFAULT_BASE+"#add";
			case "-":
				return DEFAULT_BASE+"#sub";
			case "*":
				return DEFAULT_BASE+"#mult";
			case "/":
				return DEFAULT_BASE+"#div";
			case "mod":
				return DEFAULT_BASE+"#mod";
			case "^":
				return DEFAULT_BASE+"#power";
			case ">":
				return DEFAULT_BASE+"#greater";
			case "<":
				return DEFAULT_BASE+"#less";
			case ">=":
				return DEFAULT_BASE+"#gteq";
			case "<=":
				return DEFAULT_BASE+"#leq";
			default:
				return DEFAULT_BASE+"#diff";
		}
	}

	/*abstracts builds methods*/
	public abstract void buildImportDecl(String url);
	public abstract void buildPrefixDecl(String prefix,Object ns) throws BuildException;
	public abstract void buildComputedDecl(String prefix,Object ns) throws BuildException;
	public abstract void buildViewDecl(String prefix, Object ns) throws BuildException;
	public abstract void buildBaseDecl(String base) throws BuildException;
	public abstract void buildTopDecl(String top,String flag) throws BuildException;
	public abstract void buildUnaDecl();
	public abstract ParseEvent buildSpecialAtom(String binaryPred,ArrayList<Object> listTerm);
	public abstract Object buildPredicate(String token);
	public abstract ParseEvent buildAtom(Object predicate,ArrayList<Object> listTerm);
	public abstract ParseEvent buildAtomInBody(Object predicate,ArrayList<Object> listTerm) throws BuildException;
	public abstract ParseEvent buildComputedAtom(Object predicate,ArrayList<Object> listTerm);
	public abstract ParseEvent buildEquality(Object term0,Object term1);

	public abstract Object buildFunctionalOrIriTerm(Object nameFunction,ArrayList<Object> listTermFunction,String prefix);
	public abstract Object buildSpecialFunctionalTerm(Object leftOperand,ArrayList<String> ops,ArrayList<Object> rightOperands);

	public abstract void buildNegativeConjunction(ArrayList<ArrayList<ParseEvent >> listNegConjunction);
	public abstract void buildVarList(ArrayList<Object> listTerm);
	public abstract Object buildBool(String token);
	public abstract Object buildDecimal(String token);
	public abstract Object buildDouble(String token);
	public abstract Object buildInteger(String token);
	public abstract Object buildLang(String qs,String lang);
	public abstract Object buildString(String qs);
	public abstract void EndConjunctionEvent(OBJECT_TYPE object_type);
	public abstract void buildQuery(String name);
	public abstract void buildNegativeConst(String name);
	public abstract void buildFact(ArrayList < ParseEvent > pending_events,String name);
	public abstract void buildRule(ArrayList < ParseEvent > pending_events,String name);
	public abstract Object createIRI(String s);
	public abstract Object createLiteral(Object datatype, String stringValue,String langTag);
	public abstract Object createVariable(String stringValue);

}
