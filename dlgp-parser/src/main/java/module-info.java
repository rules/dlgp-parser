module fr.lirmm.graphik.dlgp3 {
	
	requires json.simple;
	requires java.desktop;
    requires java.compiler;
    requires fr.inria.integraal.view_parser.parser;

    exports fr.lirmm.graphik.dlgp3.parser;
	
}