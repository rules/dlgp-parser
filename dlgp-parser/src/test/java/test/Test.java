package test;

import fr.lirmm.graphik.dlgp3.parser.ADlgpItemFactory;
import fr.lirmm.graphik.dlgp3.parser.BuildException;
import fr.lirmm.graphik.dlgp3.parser.DLGP2Parser;

import java.io.StringReader;
import java.util.ArrayList;

public class Test {

    private static final String s = "@view v:<src/test/resources/v.vd>\n ?(X, Y) :- v:view(X, Y).";

    public static void main(String[] args) {
        ADlgpItemFactory f = new Factory();
        DLGP2Parser parser = new DLGP2Parser(f, new StringReader(s));

        try {
            parser.document();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static class Factory extends ADlgpItemFactory {

        @Override
        public void buildViewDecl(String prefix, Object ns) throws BuildException {
        }

        @Override
        public Object createIRI(String s) {
            return s;
        }

        @Override
        public void buildImportDecl(String url) {

        }

        @Override
        public void buildPrefixDecl(String prefix, Object ns) throws BuildException {

        }

        @Override
        public void buildComputedDecl(String prefix, Object ns) throws BuildException {

        }

        @Override
        public void buildBaseDecl(String base) throws BuildException {

        }

        @Override
        public void buildTopDecl(String top, String flag) throws BuildException {

        }

        @Override
        public void buildUnaDecl() {

        }

        @Override
        public ParseEvent buildSpecialAtom(String binaryPred, ArrayList<Object> listTerm) {
            return null;
        }

        @Override
        public Object buildPredicate(String token) {
            return null;
        }

        @Override
        public ParseEvent buildAtom(Object predicate, ArrayList<Object> listTerm) {
            return null;
        }

        @Override
        public ParseEvent buildAtomInBody(Object predicate, ArrayList<Object> listTerm) throws BuildException {
            return null;
        }

        @Override
        public ParseEvent buildComputedAtom(Object predicate, ArrayList<Object> listTerm) {
            return null;
        }

        @Override
        public ParseEvent buildEquality(Object term0, Object term1) {
            return null;
        }

        @Override
        public Object buildFunctionalOrIriTerm(Object nameFunction, ArrayList<Object> listTermFunction, String prefix) {
            return null;
        }

        @Override
        public Object buildSpecialFunctionalTerm(Object leftOperand, ArrayList<String> ops, ArrayList<Object> rightOperands) {
            return null;
        }

        @Override
        public void buildNegativeConjunction(ArrayList<ArrayList<ParseEvent>> listNegConjunction) {

        }

        @Override
        public void buildVarList(ArrayList<Object> listTerm) {

        }

        @Override
        public Object buildBool(String token) {
            return null;
        }

        @Override
        public Object buildDecimal(String token) {
            return null;
        }

        @Override
        public Object buildDouble(String token) {
            return null;
        }

        @Override
        public Object buildInteger(String token) {
            return null;
        }

        @Override
        public Object buildLang(String qs, String lang) {
            return null;
        }

        @Override
        public Object buildString(String qs) {
            return null;
        }

        @Override
        public void EndConjunctionEvent(OBJECT_TYPE object_type) {

        }

        @Override
        public void buildQuery(String name) {

        }

        @Override
        public void buildNegativeConst(String name) {

        }

        @Override
        public void buildFact(ArrayList<ParseEvent> pending_events, String name) {

        }

        @Override
        public void buildRule(ArrayList<ParseEvent> pending_events, String name) {

        }

        @Override
        public Object createLiteral(Object datatype, String stringValue, String langTag) {
            return null;
        }

        @Override
        public Object createVariable(String stringValue) {
            return null;
        }
    }

}
